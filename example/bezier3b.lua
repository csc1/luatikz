tikz.within( '*' )

local bz =
	bezier3{
		p1  = p{ 0, 0 },
		pc1 = p{ 1, 2 },
		pc2 = p{ 3, 3 },
		p2  = p{ 4, 0 },
	}

draw{ line_width = 1, bz }

for t = 0, 1, 0.1
do
	local pt = bz.pt( t )
	draw{
		fill = black,
		circle{
			at = pt,
			radius = 0.05,
		}
	}

	local phit = bz.phit( t ) + math.pi / 2
	draw{
		line{
			p1 = pt,
			phi = phit,
			length = 1,
		}
	}
end
