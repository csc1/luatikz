tikz.within( '*' )

draw{
	ellipse{
		at = p{ 0, 0 },
		xradius = 3,
		yradius = 2,
	},
	ellipse{
		at = p{ 2, 1 },
		xradius = 1.5,
		yradius = 1,
	}
}
