tikz.within( '*' )

local r1 =
	rect{
		pnw = p{ 0, 3 },
		pse = p{ 3, 0 },
	}

local r2 =
	rect{
		psw  = r1.pse,
		size = p{ 1, 1 },
	}

local r3 =
	rect{
		pc   = r1.pc,
		size = p{ 1.5, 1.5 },
	}

draw{ r1, r2, r3 }
