tikz.within( '*' )

local l1 = bline{ p{ 0, 0 }, p{ 2, 3 }, bend_right = 45 }
local l2 = bline{ p{ 0, 3 }, p{ 2, 1 }, bend_left  = 30 }

draw{ draw=red,  l1 }
draw{ draw=blue, l2 }
put{
	node{
		at = l1.pc,
		anchor = south,
		rotate = l1.line.phi * 180 / math.pi,
		text = 'red',
	}
}
put{
	node{
		at = l2.pc,
		anchor = south,
		rotate = l2.line.phi * 180 / math.pi,
		text = 'blue',
	}
}
