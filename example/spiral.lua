tikz.within( '*' )

local pi  = p{ 0,    0    }
local pdx = p{ 0.25, 0    }
local pdy = p{ 0,    0.25 }

for k = 1, 20
do
	local sign = 1
	if k % 2 == 0 then sign = -1 end
	local pn  = pi + sign * k * pdx
	local pn2 = pn + sign * k * pdy
	draw{ line{ pi, pn }, line{ pn, pn2 } }
	pi = pn2
end

