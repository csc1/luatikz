tikz.within( '*' )

local p1 = p{ 1, 1 }
p1 = p{ p1.x + 2, p1.y }
-- or
p1 = p1 + p{ 2, 0 }
