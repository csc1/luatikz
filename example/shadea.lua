tikz.within( '*' )

shade{
	ball_color = gray,
	circle{
		at = p0,
		radius = 2,
	},
}
