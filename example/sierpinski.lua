tikz.within( '*' )

local s60 = math.sin( 60 / 180 * math.pi )
local c60 = math.cos( 60 / 180 * math.pi )

function fractal( ptop, len, level )
	if level > 1 then len = len / 2 end

	local pleft  = ptop + p{ -len / 2, -len * s60 }
	local pright = ptop + p{  len / 2, -len * s60 }

	if level == 1
	then
		draw{
			fill = black,
			draw = none,
			polyline{ ptop, pleft, pright, 'cycle' }
		}
	else
		fractal( ptop,   len, level - 1 )
		fractal( pleft,  len, level - 1 )
		fractal( pright, len, level - 1 )
	end
end

fractal( p0, 8, 6 )
