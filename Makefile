MAIN = luatikz

.PHONY: all clean default package

default: all

all:
	lualatex -file-line-error ${MAIN}.tex

clean:
	latexmk -c ${MAIN}.tex
	@echo cleaned!

package:
	$(eval VERSION := $(shell lua -e 'dofile("luatikz.lua");print(tikz.version)'))
	$(eval EXAMPLES := $(shell echo example/*.lua))
	tar --transform 's,^,luatikz/,' -czf luatikz-${VERSION}.tar.gz luatikz.lua luatikz.sty luatikz.tex luatikz.pdf README.txt ${EXAMPLES}
